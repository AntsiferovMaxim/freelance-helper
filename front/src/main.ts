import Vue from 'vue'
import App from './app.component.vue'
import {router} from './router'

import store from './store/index'
import './services/register-service-worker'

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
