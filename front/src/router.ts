import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/views/home.component.vue'
import Admin from '@/views/admin/admin.component.vue'
import Settings from '@/views/settings/settings.component.vue';
import Orders from '@/views/orders/orders.component.vue';
import Login from '@/views/auth/login.component.vue';
import Auth from '@/views/auth/auth.component.vue';
import Registration from '@/views/auth/registration.component.vue';
import Categories from '@/views/categories/categories.component.vue';

Vue.use(Router);

export const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            redirect: '/home'
        },
        {
            path: '/home',
            name: 'home',
            component: Home,
            children: [
                {
                    path: 'admin',
                    name: 'admin',
                    component: Admin
                },
                {
                    path: 'categories',
                    name: 'categories',
                    component: Categories
                },
                {
                    path: 'settings',
                    name: 'settings',
                    component: Settings
                },
                {
                    path: 'orders',
                    name: 'orders',
                    component: Orders
                }
            ]
        },
        {
            path: '/auth',
            name: 'auth',
            component: Auth,
            children: [
                {
                    path: 'login',
                    name: 'login',
                    component: Login
                },
                {
                    path: 'registration',
                    name: 'registration',
                    component: Registration
                }
            ]
        },
    ]
});

/*
router.beforeEach((to, from, next) => {
    let user = JSON.parse(localStorage.getItem('user'));
    if (to.path !== '/login' && !user) {
        next('/login')
    } else if (to.path === '/login' && user) {
        next({path: '/'})
    } else {
        if (to.matched.some(record => record.meta.permissions)) {
            // этот путь требует авторизации, проверяем права, если нет - перенаправляем на страницу логина
            if (!PermissionsHelper.checkPermissions(user.permissions, to.meta.permissions)) {
                next({
                    path: '/permission-denied',
                    query: {redirect: to.fullPath}
                })
            } else {
                next()
            }
        } else {
            next()
        }
    }
});
*/
