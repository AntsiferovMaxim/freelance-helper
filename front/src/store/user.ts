import Vue from 'vue';

const store = {
    id: null
};

const mutations = {
    setId(state: any, id: number) {
        Vue.set(state, 'id', id)
    }
};

const actions = {};

const getters = {};

export default {store, mutations, actions, getters, namespaced: true}