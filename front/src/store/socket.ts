import {Payload, ActionContext} from 'vuex';

class Store {
    socket: any = null;
}

const mutations = {
    setSocket(state: any, socket: Payload) {
        state.socket = socket;
    }
};

const actions = {
    setSocket(context: ActionContext<Store, any>, socket: any)  {
        context.commit('setSocket', socket);
    }
};

const getters = {
    getSocket: (state: any) => state.socket
};

export default {store: new Store(), mutations, actions, getters}