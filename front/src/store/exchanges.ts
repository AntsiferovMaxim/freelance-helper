import Vue from 'vue';
import axiosInstant from "@/services/axios-instant";

class Store {
    list: Array<object> = [];
}

const mutations = {
    setExchanges(state: any, payload: any) {
        Vue.set(state, 'list', payload);
    },
    update(state: any, payload: any) {
        Vue.set(state.list, payload.index, payload.data);
    }
};

const actions = {
    async getAll(context: any) {
        try {
            const res = await axiosInstant.get('/exchanges');

            context.commit('setExchanges', res.data.data);
        } catch (e) {
            console.log(e);
        }
    },
    async update(context: any, payload: any) {
        try {
            const res = await axiosInstant.put('/exchanges/' + payload.data.exchanges_id, {
                data: payload.data
            });

            context.commit('update', payload);
        } catch (e) {
            console.log(e);
        }
    }
};

const getters = {
    all: (state: any) => state.list
};

export default {store: new Store(), mutations, actions, getters, namespaced: true}