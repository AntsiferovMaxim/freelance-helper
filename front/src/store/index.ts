import Vuex from 'vuex'
import Vue from "vue";

import socket from './socket';
import categories from './categories';
import auth from './auth';
import user from './user';
import exchanges from './exchanges';
import parse from './parse';
import orders from './orders';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        socket,
        categories,
        auth,
        parse,
        orders,
        user,
        exchanges
    }
})
