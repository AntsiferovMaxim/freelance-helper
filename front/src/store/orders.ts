import Vue from 'vue';
import uuid from 'uuid/v1';

class Store {
    orders: Array<any> = [];
}

const mutations = {
    add: (state: any, order: any) => {
        !state.orders && Vue.set(state, 'orders', []);

        Vue.set(state.orders, state.orders.length, {
            ...order,
            new: true,
            id: uuid()
        });
    },
    setViewed: (state: any, index: number) => {
        Vue.set(state.orders, index, {
            ...state.orders[index],
            new: false
        });
    },
    remove: (state: any, index: number) => {
        Vue.set(state, 'orders', state.orders.filter((item: any, itemIndex: number) => itemIndex !== index))
    }
};

const actions = {

};

const getters = {
    getOrders: (state: any) => state.orders,
    getNewCount: (state: any) => state.orders ? state.orders.filter((order: any) => order.new).length : 0
};

export default {store: new Store(), mutations, actions, getters, namespaced: true}