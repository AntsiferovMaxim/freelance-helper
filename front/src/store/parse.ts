import Vue from 'vue';

class Store {
    isParsing: boolean = false;
}

const mutations = {
    setParsingStatus(state: any, status: boolean) {
        Vue.set(state, 'isParsing', status);
    }
};

const actions = {};

const getters = {
    getParsingStatus: (state: any) => state.isParsing
};

export default {store: new Store(), mutations, actions, getters, namespaced: true}