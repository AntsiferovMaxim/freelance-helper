import {Payload, ActionContext} from 'vuex';
import Vue from 'vue';
import findIndex from 'lodash/findIndex';
import axiosInstant from '@/services/axios-instant';

const LOADING = {
    error: -1,
    initial: 0,
    loading: 1,
    ready: 2,
};

class Store {
    categories: Array<any> = [];
    subcategories: Array<any> = [];
    selectedCategories: Array<number>;
    loading: number = LOADING.initial;
}

const mutations = {
    setCategories(state: any, categories: Payload) {
        Vue.set(state, 'categories', categories);
    },
    setSubcategories(state: any, subcategories: Payload) {
        Vue.set(state, 'subcategories', subcategories);
    },
    setLoadingStatus(state: any, status: number) {
        Vue.set(state, 'loading', status);
    },
    setSelectedCategories(state: any) {
        Vue.set(state, 'selectedCategories', []);
    },
    addCategory(state: any, category: any) {
        Vue.set(state, 'selectedCategories', [...state.selectedCategories, category]);
    },
    deleteCategory(state: any, index: number) {
        const newArray = [...state.selectedCategories];
        newArray.splice(index, 1);

        Vue.set(state, 'selectedCategories', [...newArray]);
    }
};

const actions = {
    async getCategories(context: ActionContext<Store, any>, payload: any) {
        context.commit('setLoadingStatus', LOADING.loading);

        try {
            const res = await axiosInstant({
                method: 'GET',
                url: '/categories',
                params: payload
            });

            context.commit('setLoadingStatus', LOADING.ready);
            context.commit('setCategories', res.data);
            context.commit('setSubcategories', undefined);
        } catch (e) {
            context.commit('setLoadingStatus', LOADING.error)
        }
    },
    async getSubcategories(context: any, payload: any) {
        context.commit('setLoadingStatus', LOADING.loading);

        try {
            const res = await axiosInstant({
                method: 'GET',
                url: '/categories',
                params: payload
            });

            context.commit('setLoadingStatus', LOADING.ready);
            context.commit('setSubcategories', res.data);
        } catch (e) {
            context.commit('setLoadingStatus', LOADING.error)
        }
    },
    selectCategory(context: ActionContext<Store, any>, category: any) {
        if (!context.state.selectedCategories) {
            context.commit('setSelectedCategories');
        }

        const index = findIndex(context.state.selectedCategories, (item: any) => {
            return item.id === category.id;
        });

        if (index < 0) {
            context.commit('addCategory', category);
        } else {
            context.commit('deleteCategory', index);
        }
    },
    categoriesByExchange(context: any, payload: any) {
        return context.state.categories.filter(
            (category: any) => {
                if (payload.parent) {
                    return category.exchanges_id === payload.exchanges_id &&
                        category.parent_id === payload.parent
                } else {
                    return category.exchanges_id === payload.exchanges_id
                }
            }
        )
    }
};

const getters = {
    categories: (state: any): Array<any> => state.categories,
    subcategories: (state: any) => state.subcategories,
    categoriesLoading: (state: any): number => state.loading,
    selectedCategoriesId: (state: any): Array<number> => state.selectedCategories ? state.selectedCategories.map((item: any) => item.id) : [],
    isSelected: (state: any) => (id: number) => state.selectedCategories ? findIndex(state.selectedCategories, (item: any) => item.id === id) >= 0 : false,
    selectedCategories: (state: any) => state.selectedCategories
};

export default {store: new Store(), mutations, actions, getters, namespaced: true}