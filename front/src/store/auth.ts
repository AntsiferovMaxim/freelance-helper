import Vue from 'vue';
import uuid from 'uuid/v1';
import axiosInstant from '@/services/axios-instant';
import URL from '@/constants/api-url';
import jwtDecode from 'jwt-decode';

const store = {
    clientId: null,
    accessToken: null,
    refreshToken: null,
    expiresIn: null
};

const mutations = {
    initializationAuthData(state: any, data: any) {
        for (let key in data) {
            Vue.set(state, key, data[key]);
        }
    }
};

const actions = {
    async registration(context: any, payload: any) {
        let res;

        try {
            res = await axiosInstant.post(URL.auth.registration, {data: {email: payload.email, password: payload.password}});
        } catch (e) {
            return e.response.data;
        }

        return res.data;
    },

    async login(context: any, payload: any) {
        let res;
        const client_id = uuid();

        try {
            res = await axiosInstant.post(URL.auth.login, {data: {email: payload.email, password: payload.password, client_id}});
        } catch (e) {
            return e.response.data;
        }

        if (!res.data.error) {
            context.commit('initializationAuthData', {
                ...res.data.data,
                clientId: client_id
            });
        }


        const jwt = <any>jwtDecode(res.data.data.access_token);

        return {
            ...res.data,
            id: jwt.id
        };
    }
};

const getters = {
    id: (state: any) => state.id
};

export default {store, mutations, actions, getters, namespaced: true}