export default {
    auth: {
        registration: '/auth/registration',
        login: '/auth/login'
    }
}