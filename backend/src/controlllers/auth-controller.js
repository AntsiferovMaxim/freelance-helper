const bcrypt = require("bcryptjs");

const {asyncJWT} = require("../libs/async-jwt");
const {getUnixTimestamp} = require("../libs/time");
const {UserModel} = require("../db/models/user-model");

module.exports = class AuthController {
    static async registration(req, res) {
        try {
            const {password, email} = req.body.data;

            //TODO Add check on string
            if (!password || password.lenght < 6) return res.status(400).send({msg: `Слишком короткий пароль`, error: true});
            if (!email || email.length === 0) return res.status(400).send({msg: `Введите emaill`, error: true});

            //TODO Add email validation
            const user = await UserModel.getByEmail(email);

            if (user) return res.status(409).send({msg: `Пользователь c почтой ${email} уже существует`, error: true});

            const salt = bcrypt.genSaltSync(10);
            const hashedPassword = bcrypt.hashSync(password, salt);

            await UserModel.save({ password: hashedPassword , email});

            res.send({msg: "Вы успешно зарегистрированны", error: false})
        } catch (e) {
            console.log(e);
            res.status(500).send({msg: "Что-то пошло не так, сервер споткнулся(", error: true})
        }
    }

    static  async login(req, res) {
        try {
            const {email, password, client_id} = req.body.data;

            if (email.length === 0) return res.status(400).send({msg: "Вы не ввели логин", error: true});
            if (password.length === 0) return res.status(400).send({msg: "Вы не ввели email", error: true});

            const user = await UserModel.getByEmail(email);

            if (!user) {
                return res.send({msg: "Такого пользователя не существует", error: true});
            }

            const token = await UserModel.getRefreshTokenByClientId(client_id);

            try {
                const userId = (await asyncJWT.verify(token, process.env.SECRET)).id;

                if (userId) {
                    return res.send({msg: "Вы уже авторизованы", error: true});
                }
            } catch (e) {
                await UserModel.deleteRefreshToken(client_id);
            }

            const passwordIsValid = bcrypt.compareSync(password, user.password);

            if (!passwordIsValid) {
                return res.status(403).send({msg: "Не верный пароль/почта", error: true});
            }

            const userAgent = req.headers['user-agent'];

            const accessToken = genAccessToken({id: user.user_id});
            const refreshToken = genRefreshToken({id: user.user_id});
            await UserModel.addRefreshToken({
                user_id: user.user_id,
                refresh_token: refreshToken,
                user_agent: userAgent,
                client_id
            });

            res.send({
                data: {
                    access_token: accessToken,
                    refresh_token: refreshToken,
                    expires_in: getUnixTimestamp() + process.env.ACCESS_TOKEN_EXPIRES
                },
                msg: "Вы авторизованы",
                error: false
            })
        } catch (e) {
            console.error("auth-route:login\n", e);
            res.send({msg: "Ошибка сервера", error: true})
        }
    }

    static async logout(req, res) {
        try {
            const data = await UserModel.getRefreshTokenByClientId(req.params.client_id);

            if (data) {
                await UserModel.deleteRefreshToken(req.params.client_id);
                return res.send({msg: "Вы успешно вышли"});
            } else {
                return res.send({msg: "Вы не авторизованы"});
            }
        } catch (e) {
            console.log(e);
            return res.status(500).send({msg: "Что-то пошло не так :("});
        }
    }

    static async refreshToken(req, res) {
        try {
            const {refresh_token, client_id} = req.body.data;
            let userId;

            try {
                userId = (await asyncJWT.verify(refresh_token, process.env.SECRET)).id;
            } catch (e) {
                console.log(e);
                return res.send({msg: "Время авторизации закончилось, нужно перелогиниться"});
            }

            const data = await UserModel.getRefreshToken(client_id, refresh_token);

            if (data) {
                const newRefreshToken = genRefreshToken({userId});
                const newAccessToken = genAccessToken({userId});

                await UserModel.updateRefreshToken(client_id, newRefreshToken);

                return res.send({
                    accessToken: newAccessToken,
                    refreshToken: newRefreshToken,
                    expiresIn: getUnixTimestamp() + process.env.ACCESS_TOKEN_EXPIRES
                });
            } else {
                res.status(404).send({msg: "У вас какая-то хуйня с токеном, перелогинтесь"});
            }
        } catch (e) {
            console.log(e);
            res.status(500).send({msg: "Произошла какая-то ошибка :Ж"});
        }
    }

    static async authMiddleware(req, res, next) {
        const [bearer, token] = req.headers.authorization && req.headers.authorization.split(' ');

        try {
            let data;

            try {
                data = await asyncJWT.verify(token, process.env.SECRET);
            } catch (e) {
                console.log(e);
                return res.status(409).send({msg: "Закончилось время авторизации"});
            }

            req.body.user = {id: data.id};
            next();
        } catch (e) {
            console.log(e);
            res.status(403).send({msg: "Нет доступа"});
        }
    }
};

function genAccessToken(payload) {
    return asyncJWT.sign(payload, process.env.SECRET, { expiresIn: process.env.ACCESS_TOKEN_EXPIRES });
}

function genRefreshToken(payload) {
    return asyncJWT.sign(payload, process.env.SECRET, { expiresIn: process.env.REFRESH_TOKEN_EXPIRES });
}