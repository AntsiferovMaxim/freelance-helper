const {CategoryModel} = require('../db/models/category-model');

exports.CategoriesController = class CategoriesController {
    static async getCategories(req, res) {
        try {
            const where = {};

            if (req.query.exchanges_id)
                where.exchanges_id = req.query.exchanges_id;

            if (req.query.level)
                where.level = req.query.level;

            if (req.query.parent_id)
                where.parent_id = req.query.parent_id === 'null' ? null : req.query.parent_id;

            const categories = await CategoryModel.getBy(where);

            res.send(categories);
        } catch (e) {
            console.log(e);

            res.status(500).send({msg: 'Server error'})
        }
    }
};