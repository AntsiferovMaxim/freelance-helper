const {ExchangesModel} = require('../db/models/exchange-model');

module.exports = class ExchangesController {
    static async getAllExchanges(req, res) {
        try {
            const r = await ExchangesModel.getAll();

            res.send({data: r});
        } catch (e) {
            console.error(e);
            res.status(500).send({msg: 'Server error'});
        }
    }

    static async updateExchange(req, res) {
        try {
            const data = req.body.data;

            const r = await ExchangesModel.updateById(req.params.id, data);
            res.send({msg: 'Updated: ' + r})
        } catch (e) {
            console.log(e);
            res.status(500).send({msg: 'Server error'});
        }
    }
};