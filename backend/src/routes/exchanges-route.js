const router = require("express").Router();
const ExchangesController = require('../controlllers/exchanges-controller');

module.exports = router
    .get('/', ExchangesController.getAllExchanges)
    .put('/:id', ExchangesController.updateExchange);