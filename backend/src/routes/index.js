const router = require('express').Router();

const authRoutes = require("./auth-route");
const exchangesRoutes = require("./exchanges-route");
const categoriesRoutes = require("./categories-route");

module.exports = router
    .use('/auth/', authRoutes)
    .use('/exchanges/', exchangesRoutes)
    .use('/categories/', categoriesRoutes)
    .get('/', function (req, res) {
        res.send('hui')
    });