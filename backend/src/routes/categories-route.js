const router = require("express").Router();
const {CategoriesController} = require('../controlllers/categories-controller');

module.exports = router
    .get('/', CategoriesController.getCategories);