const router = require("express").Router();
const AuthController = require("../controlllers/auth-controller");

module.exports = router
    .post("/registration/", AuthController.registration)
    .post("/login/", AuthController.login)
    .get("/logout/:client_id", AuthController.authMiddleware, AuthController.logout)
    .post("/refresh-token/", AuthController.refreshToken);