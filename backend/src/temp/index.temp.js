const express = require('express');
const path = require('path');
const morgan = require('morgan');
const cron = require('node-cron');
const bodyParser = require("body-parser");
const {findIndex} = require('lodash/array');
const {parseOrders, parseCategories} = require('./libs/parsers/weblancer');
const routes = require("./routes");

const app = express();
const port = process.env.PORT || 3000;

let categories = require('./temp/categories');
let categoriesQueue = [];
let activeUsers = [];

const ordersHandler = {
    set: function (array, prop, value) {
        if (prop === 'length') {
            array[prop] = value;

            return true;
        }

        const index = findIndex(array, item => item.link === value.link);

        if (index < 0) {
            array[prop] = value;

            const filteredCategories = categoriesQueue.filter(category => category.categoryId === value.categoryId);

            filteredCategories.forEach(item => {
                const index = findIndex(activeUsers, user => user.userId === item.userId);

                if (index >= 0) {
                    activeUsers[index].socket.emit('new-order', value);
                }
            });
        }

        return true;
    }
};

const domain = 'https://www.weblancer.net';

const orders = new Proxy([], ordersHandler);

cron.schedule('*/30 * * * * *', async function () {
    const prepareCategories = categoriesQueue.map(({categoryId}) => {
        const index = findIndex(categories, (item) => item.id === categoryId);

        return {link: domain + categories[index].link, categoryId: categories[index].id};
    });

    const parsedOrders = await Promise.all(prepareCategories.map(item => parseOrders(item)));

    parsedOrders.forEach(ordersByCategory => {
        ordersByCategory.orders.forEach(order => {
            orders.push({
                ...order,
                categoryId: ordersByCategory.categoryId,
                link: domain + order.link
            });
        });
    })
});

const pathToStatic = path.resolve(__dirname, '../public');

const server = app
    .use(express.static(pathToStatic))
    .use(bodyParser.urlencoded({extended: false}))
    .use(bodyParser.json())
    .use(function (req, res, next) {
        next();
    })
    .use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    })
    .use(morgan(':method :url :status :res[content-length] :response-time ms'))
    .use(routes)
    .get('/categories', async (req, res) => {
        const groupedCategories = categories.reduce((acc, {name, link, id, parent}) => {
            const index = findIndex(acc, (item) => item.link === parent.link);

            if (index < 0) {
                acc.push({
                    name: parent.name,
                    link: parent.link,
                    subcategories: [{name, link, id}]
                });
            } else {
                acc[index].subcategories.push({name, link, id});
            }

            return acc;
        }, []);

        res.send({categories: groupedCategories});
    })
    .listen(port, async () => {
        console.log(`Api starting in http://localhost:${port}/`);

        // categories = await parseCategories();
    });

const io = require('socket.io')(server, {origins: '*:*'});

io.on('connection', function (socket) {
    let id;

    console.log('connect');

    socket.on('parse', function ({categories: userCategories, userId}) {
        activeUsers.push({userId, socket});
        console.log('Add new user');

        id = userId;

        userCategories && userCategories.forEach(categoryId => categoriesQueue.push({categoryId: +categoryId, userId}))
    });

    socket.on('parse-end', function (data) {
        if (data) {
            console.log('parse-end');

            activeUsers = activeUsers.filter(item => item.userId !== data.userId);
            categoriesQueue = categoriesQueue.filter(item => item.userId !== data.userId);
        }
    });

    socket.on('disconnect', function () {
        console.log('disconnect');

        activeUsers = activeUsers.filter(item => item.userId !== id);
        categoriesQueue = categoriesQueue.filter(item => item.userId !== id);
    });
});
