module.exports = [{
    "name": "Наполнение сайтов",
    "link": "/jobs/napolnenie-sajtov-35/",
    "id": 35,
    "parent": {"name": "Администрирование сайтов", "link": "/jobs/administrirovanie-sajtov-6/"}
}, {
    "name": "Системное администрирование",
    "link": "/jobs/sistemnoe-administrirovanie-54/",
    "id": 54,
    "parent": {"name": "Администрирование сайтов", "link": "/jobs/administrirovanie-sajtov-6/"}
}, {
    "name": "Служба поддержки",
    "link": "/jobs/sluzhba-podderzhki-56/",
    "id": 56,
    "parent": {"name": "Администрирование сайтов", "link": "/jobs/administrirovanie-sajtov-6/"}
}, {
    "name": "Архитектура зданий",
    "link": "/jobs/arhitektura-zdanij-75/",
    "id": 75,
    "parent": {"name": "Архитектура и Инжиниринг", "link": "/jobs/arhitektura-i-inzhiniring-74/"}
}, {
    "name": "Интерьеры и Экстерьеры",
    "link": "/jobs/interyery-i-eksteryery-15/",
    "id": 15,
    "parent": {"name": "Архитектура и Инжиниринг", "link": "/jobs/arhitektura-i-inzhiniring-74/"}
}, {
    "name": "Ландшафтный дизайн",
    "link": "/jobs/landshaftnyj-dizajn-76/",
    "id": 76,
    "parent": {"name": "Архитектура и Инжиниринг", "link": "/jobs/arhitektura-i-inzhiniring-74/"}
}, {
    "name": "Машиностроение",
    "link": "/jobs/mashinostroenie-77/",
    "id": 77,
    "parent": {"name": "Архитектура и Инжиниринг", "link": "/jobs/arhitektura-i-inzhiniring-74/"}
}, {
    "name": "Чертежи и Схемы",
    "link": "/jobs/chertezhi-i-shemy-78/",
    "id": 78,
    "parent": {"name": "Архитектура и Инжиниринг", "link": "/jobs/arhitektura-i-inzhiniring-74/"}
}, {
    "name": "Анимация",
    "link": "/jobs/animatciya-39/",
    "id": 39,
    "parent": {"name": "Аудио, Видео и Мультимедиа", "link": "/jobs/audio-video-i-mulytimedia-4/"}
}, {
    "name": "Аудиомонтаж",
    "link": "/jobs/audiomontazh-85/",
    "id": 85,
    "parent": {"name": "Аудио, Видео и Мультимедиа", "link": "/jobs/audio-video-i-mulytimedia-4/"}
}, {
    "name": "Видеомонтаж",
    "link": "/jobs/videomontazh-41/",
    "id": 41,
    "parent": {"name": "Аудио, Видео и Мультимедиа", "link": "/jobs/audio-video-i-mulytimedia-4/"}
}, {
    "name": "Музыка и Звуки",
    "link": "/jobs/muzyka-i-zvuki-40/",
    "id": 40,
    "parent": {"name": "Аудио, Видео и Мультимедиа", "link": "/jobs/audio-video-i-mulytimedia-4/"}
}, {
    "name": "Озвучивание",
    "link": "/jobs/ozvuchivanie-42/",
    "id": 42,
    "parent": {"name": "Аудио, Видео и Мультимедиа", "link": "/jobs/audio-video-i-mulytimedia-4/"}
}, {
    "name": "Презентации",
    "link": "/jobs/prezentatcii-60/",
    "id": 60,
    "parent": {"name": "Аудио, Видео и Мультимедиа", "link": "/jobs/audio-video-i-mulytimedia-4/"}
}, {
    "name": "Баннеры",
    "link": "/jobs/bannery-8/",
    "id": 8,
    "parent": {"name": "Веб-дизайн и Интерфейсы", "link": "/jobs/veb-dizajn-i-interfejsy-1/"}
}, {
    "name": "Дизайн мобильных приложений",
    "link": "/jobs/dizajn-mobilynyh-prilozhenij-82/",
    "id": 82,
    "parent": {"name": "Веб-дизайн и Интерфейсы", "link": "/jobs/veb-dizajn-i-interfejsy-1/"}
}, {
    "name": "Дизайн сайтов",
    "link": "/jobs/dizajn-sajtov-9/",
    "id": 9,
    "parent": {"name": "Веб-дизайн и Интерфейсы", "link": "/jobs/veb-dizajn-i-interfejsy-1/"}
}, {
    "name": "Иконки и Пиксель-арт",
    "link": "/jobs/ikonki-i-piksely-art-14/",
    "id": 14,
    "parent": {"name": "Веб-дизайн и Интерфейсы", "link": "/jobs/veb-dizajn-i-interfejsy-1/"}
}, {
    "name": "Интерфейсы игр и программ",
    "link": "/jobs/interfejsy-igr-i-programm-13/",
    "id": 13,
    "parent": {"name": "Веб-дизайн и Интерфейсы", "link": "/jobs/veb-dizajn-i-interfejsy-1/"}
}, {
    "name": "HTML-верстка",
    "link": "/jobs/html-verstka-32/",
    "id": 32,
    "parent": {"name": "Веб-программирование и Сайты", "link": "/jobs/veb-programmirovanie-i-sajty-3/"}
}, {
    "name": "Веб-программирование",
    "link": "/jobs/veb-programmirovanie-31/",
    "id": 31,
    "parent": {"name": "Веб-программирование и Сайты", "link": "/jobs/veb-programmirovanie-i-sajty-3/"}
}, {
    "name": "Интернет-магазины",
    "link": "/jobs/internet-magaziny-61/",
    "id": 61,
    "parent": {"name": "Веб-программирование и Сайты", "link": "/jobs/veb-programmirovanie-i-sajty-3/"}
}, {
    "name": "Сайты «под ключ»",
    "link": "/jobs/sajty-pod-klyuch-58/",
    "id": 58,
    "parent": {"name": "Веб-программирование и Сайты", "link": "/jobs/veb-programmirovanie-i-sajty-3/"}
}, {
    "name": "Системы управления (CMS)",
    "link": "/jobs/sistemy-upravleniya-cms-34/",
    "id": 34,
    "parent": {"name": "Веб-программирование и Сайты", "link": "/jobs/veb-programmirovanie-i-sajty-3/"}
}, {
    "name": "Тестирование сайтов",
    "link": "/jobs/testirovanie-sajtov-37/",
    "id": 37,
    "parent": {"name": "Веб-программирование и Сайты", "link": "/jobs/veb-programmirovanie-i-sajty-3/"}
}, {
    "name": "3D-графика",
    "link": "/jobs/3d-grafika-12/",
    "id": 12,
    "parent": {"name": "Графика и Фотография", "link": "/jobs/grafika-i-fotografiya-79/"}
}, {
    "name": "Иллюстрации и Рисунки",
    "link": "/jobs/illyustratcii-i-risunki-11/",
    "id": 11,
    "parent": {"name": "Графика и Фотография", "link": "/jobs/grafika-i-fotografiya-79/"}
}, {
    "name": "Обработка фотографий",
    "link": "/jobs/obrabotka-fotografij-21/",
    "id": 21,
    "parent": {"name": "Графика и Фотография", "link": "/jobs/grafika-i-fotografiya-79/"}
}, {
    "name": "Фотосъемка",
    "link": "/jobs/fotosyemka-20/",
    "id": 20,
    "parent": {"name": "Графика и Фотография", "link": "/jobs/grafika-i-fotografiya-79/"}
}, {
    "name": "Шрифты",
    "link": "/jobs/shrifty-87/",
    "id": 87,
    "parent": {"name": "Графика и Фотография", "link": "/jobs/grafika-i-fotografiya-79/"}
}, {
    "name": "Верстка полиграфии",
    "link": "/jobs/verstka-poligrafii-10/",
    "id": 10,
    "parent": {"name": "Полиграфия и Айдентика", "link": "/jobs/poligrafiya-i-ajdentika-80/"}
}, {
    "name": "Дизайн продукции",
    "link": "/jobs/dizajn-produktcii-18/",
    "id": 18,
    "parent": {"name": "Полиграфия и Айдентика", "link": "/jobs/poligrafiya-i-ajdentika-80/"}
}, {
    "name": "Логотипы и Знаки",
    "link": "/jobs/logotipy-i-znaki-7/",
    "id": 7,
    "parent": {"name": "Полиграфия и Айдентика", "link": "/jobs/poligrafiya-i-ajdentika-80/"}
}, {
    "name": "Наружная реклама",
    "link": "/jobs/naruzhnaya-reklama-17/",
    "id": 17,
    "parent": {"name": "Полиграфия и Айдентика", "link": "/jobs/poligrafiya-i-ajdentika-80/"}
}, {
    "name": "Фирменный стиль",
    "link": "/jobs/firmennyj-stil-19/",
    "id": 19,
    "parent": {"name": "Полиграфия и Айдентика", "link": "/jobs/poligrafiya-i-ajdentika-80/"}
}, {
    "name": "1С-программирование",
    "link": "/jobs/1s-programmirovanie-83/",
    "id": 83,
    "parent": {"name": "Программирование ПО и систем", "link": "/jobs/programmirovanie-po-i-sistem-2/"}
}, {
    "name": "Базы данных",
    "link": "/jobs/bazy-dannyh-25/",
    "id": 25,
    "parent": {"name": "Программирование ПО и систем", "link": "/jobs/programmirovanie-po-i-sistem-2/"}
}, {
    "name": "Встраиваемые системы",
    "link": "/jobs/vstraivaemye-sistemy-84/",
    "id": 84,
    "parent": {"name": "Программирование ПО и систем", "link": "/jobs/programmirovanie-po-i-sistem-2/"}
}, {
    "name": "ПО для мобильных устройств",
    "link": "/jobs/po-dlya-mobilynyh-ustrojstv-28/",
    "id": 28,
    "parent": {"name": "Программирование ПО и систем", "link": "/jobs/programmirovanie-po-i-sistem-2/"}
}, {
    "name": "Прикладное ПО",
    "link": "/jobs/prikladnoe-po-23/",
    "id": 23,
    "parent": {"name": "Программирование ПО и систем", "link": "/jobs/programmirovanie-po-i-sistem-2/"}
}, {
    "name": "Разработка игр",
    "link": "/jobs/razrabotka-igr-26/",
    "id": 26,
    "parent": {"name": "Программирование ПО и систем", "link": "/jobs/programmirovanie-po-i-sistem-2/"}
}, {
    "name": "Системное программирование",
    "link": "/jobs/sistemnoe-programmirovanie-24/",
    "id": 24,
    "parent": {"name": "Программирование ПО и систем", "link": "/jobs/programmirovanie-po-i-sistem-2/"}
}, {
    "name": "Тестирование ПО",
    "link": "/jobs/testirovanie-po-29/",
    "id": 29,
    "parent": {"name": "Программирование ПО и систем", "link": "/jobs/programmirovanie-po-i-sistem-2/"}
}, {
    "name": "Контекстная реклама",
    "link": "/jobs/kontekstnaya-reklama-88/",
    "id": 88,
    "parent": {"name": "Продвижение сайтов (SEO)", "link": "/jobs/prodvizhenie-sajtov-seo-81/"}
}, {
    "name": "Поисковые системы (SEO)",
    "link": "/jobs/poiskovye-sistemy-seo-36/",
    "id": 36,
    "parent": {"name": "Продвижение сайтов (SEO)", "link": "/jobs/prodvizhenie-sajtov-seo-81/"}
}, {
    "name": "Социальные сети (SMM и SMO)",
    "link": "/jobs/sotcialynye-seti-smm-i-smo-89/",
    "id": 89,
    "parent": {"name": "Продвижение сайтов (SEO)", "link": "/jobs/prodvizhenie-sajtov-seo-81/"}
}, {
    "name": "Копирайтинг",
    "link": "/jobs/kopirajting-48/",
    "id": 48,
    "parent": {"name": "Тексты и Переводы", "link": "/jobs/teksty-i-perevody-5/"}
}, {
    "name": "Нейминг и Слоганы",
    "link": "/jobs/nejming-i-slogany-86/",
    "id": 86,
    "parent": {"name": "Тексты и Переводы", "link": "/jobs/teksty-i-perevody-5/"}
}, {
    "name": "Переводы",
    "link": "/jobs/perevody-50/",
    "id": 50,
    "parent": {"name": "Тексты и Переводы", "link": "/jobs/teksty-i-perevody-5/"}
}, {
    "name": "Продающие тексты",
    "link": "/jobs/prodayushie-teksty-95/",
    "id": 95,
    "parent": {"name": "Тексты и Переводы", "link": "/jobs/teksty-i-perevody-5/"}
}, {
    "name": "Редактирование и Корректура",
    "link": "/jobs/redaktirovanie-i-korrektura-49/",
    "id": 49,
    "parent": {"name": "Тексты и Переводы", "link": "/jobs/teksty-i-perevody-5/"}
}, {
    "name": "Рерайтинг",
    "link": "/jobs/rerajting-94/",
    "id": 94,
    "parent": {"name": "Тексты и Переводы", "link": "/jobs/teksty-i-perevody-5/"}
}, {
    "name": "Стихи, Песни и Проза",
    "link": "/jobs/stihi-pesni-i-proza-52/",
    "id": 52,
    "parent": {"name": "Тексты и Переводы", "link": "/jobs/teksty-i-perevody-5/"}
}, {
    "name": "Сценарии",
    "link": "/jobs/stcenarii-96/",
    "id": 96,
    "parent": {"name": "Тексты и Переводы", "link": "/jobs/teksty-i-perevody-5/"}
}, {
    "name": "Транскрибация",
    "link": "/jobs/transkribatciya-97/",
    "id": 97,
    "parent": {"name": "Тексты и Переводы", "link": "/jobs/teksty-i-perevody-5/"}
}, {
    "name": "Подбор персонала (HR)",
    "link": "/jobs/podbor-personala-hr-91/",
    "id": 91,
    "parent": {"name": "Управление и Менеджмент", "link": "/jobs/upravlenie-i-menedzhment-92/"}
}, {
    "name": "Управление продажами",
    "link": "/jobs/upravlenie-prodazhami-98/",
    "id": 98,
    "parent": {"name": "Управление и Менеджмент", "link": "/jobs/upravlenie-i-menedzhment-92/"}
}, {
    "name": "Управление проектами",
    "link": "/jobs/upravlenie-proektami-65/",
    "id": 65,
    "parent": {"name": "Управление и Менеджмент", "link": "/jobs/upravlenie-i-menedzhment-92/"}
}, {
    "name": "Контрольные, Задачи и Тесты",
    "link": "/jobs/kontrolynye-zadachi-i-testy-99/",
    "id": 99,
    "parent": {"name": "Учеба и Репетиторство", "link": "/jobs/ucheba-i-repetitorstvo-93/"}
}, {
    "name": "Рефераты, Курсовые и Дипломы",
    "link": "/jobs/referaty-kursovye-i-diplomy-47/",
    "id": 47,
    "parent": {"name": "Учеба и Репетиторство", "link": "/jobs/ucheba-i-repetitorstvo-93/"}
}, {
    "name": "Уроки и Репетиторство",
    "link": "/jobs/uroki-i-repetitorstvo-101/",
    "id": 101,
    "parent": {"name": "Учеба и Репетиторство", "link": "/jobs/ucheba-i-repetitorstvo-93/"}
}, {
    "name": "Бухгалтерские услуги",
    "link": "/jobs/buhgalterskie-uslugi-72/",
    "id": 72,
    "parent": {"name": "Экономика, Финансы и Право", "link": "/jobs/ekonomika-finansy-i-pravo-62/"}
}, {
    "name": "Финансовые услуги",
    "link": "/jobs/finansovye-uslugi-73/",
    "id": 73,
    "parent": {"name": "Экономика, Финансы и Право", "link": "/jobs/ekonomika-finansy-i-pravo-62/"}
}, {
    "name": "Юридические услуги",
    "link": "/jobs/yuridicheskie-uslugi-71/",
    "id": 71,
    "parent": {"name": "Экономика, Финансы и Право", "link": "/jobs/ekonomika-finansy-i-pravo-62/"}
}];