const request = require('../net/request-html');
const cheerio = require('cheerio');

async function parseOrders({categoryId, link}) {
    const res = await request(link);
    const orders = [];
    const $ = cheerio.load(res.body);

    const rows = $('.cols_table .row');

    rows.each(function () {
        const linkElement = $(this).find('.title a');
        const title = linkElement.text();
        const link = linkElement.attr('href');

        if (title.trim() !== '') {
            orders.push({
                title,
                link,
                description: $(this).find('p.text_field').text(),
                price: $(this).find('div.amount').text(),
                bid: $(this).find('div.text_field').text().trim().split(' ')[0]
            });
        }
    });

    return {categoryId, orders};
}

async function parseSubcategories(link) {
    const res = await request(link);
    const $ = cheerio.load(res.body);

    const categories = [];

    const rootCategory = Array.from($(`.category_tree > li`)).slice(1);

    rootCategory.forEach(item => {
        const tag = $(item).children().first().prop('tagName').toLowerCase();

        if (tag === 'b') {
            const subNodes = Array.from($(item).find('ul li a'));

            subNodes.forEach(sub => {
                categories.push({
                    name: $(sub).text().trim(),
                    link: $(sub).attr('href'),
                });
            })
        }
    });

    return categories;
}

async function parseCategories() {
    const domain = 'https://www.weblancer.net';
    const url = '/jobs/';
    const res = await request(domain + url);
    const $ = cheerio.load(res.body);

    const categoriesNodes = Array.from($('.category_tree > li')).slice(1);

    return Promise.all(categoriesNodes.map(async item => {
        const child = $(item).children().first();
        const href = child.attr('href');

        const subcategories = await parseSubcategories(domain + href);

        return {
            name: child.text().trim(),
            link: href,
            subcategories
        }
    }));
}

module.exports = {parseOrders, parseCategories};