const weblancer = require('./weblancer');
const youdo = require('./youdo');

const parsers = [];
parsers[1] = weblancer;
parsers[2] = youdo;

module.exports = parsers;