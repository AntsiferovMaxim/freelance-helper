const request = require('../net/request-html');
const cheerio = require('cheerio');

async function parseCategories() {
    const domain = 'https://youdo.com';
    const all = '/tasks-all-any-all-1';

    const res = await request(domain + all);
    const $ = cheerio.load(res.body);

    const categoriesNodes = Array.from($('.js-filter-categories .category-link')).slice(1);

    const subcategories = categoriesNodes.map((node) => {
        return {
            name: $(node).text().trim(),
            link: $(node).attr('href')
        }
    });

    return [{
        name: 'Все',
        link: all,
        subcategories
    }]
}

module.exports = {parseCategories};