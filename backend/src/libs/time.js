module.exports = {
    getUnixTimestamp() {
        return new Date() / 1000 | 0
    }
};