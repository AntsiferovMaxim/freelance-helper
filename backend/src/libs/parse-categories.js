const {CategoryModel} = require('../db/models/category-model');
const {ExchangesModel} = require('../db/models/exchange-model');

const exchangesParsers = require('./parsers');

function saveCategories(categories, exchangeId, parent, level = 0) {
    const categoryModel = new CategoryModel(exchangeId);

    categories.forEach(async category => {
        try {
            const categoryId = await categoryModel.saveOrUpdate({
                name: category.name,
                link: category.link,
                parent_id: parent,
                level
            });

            category.subcategories && saveCategories(category.subcategories, exchangeId, categoryId, level + 1);
        } catch (e) {
            console.log(e)
        }
    })
}

module.exports = async function parse() {
    const exchanges = await ExchangesModel.getAll(['exchanges_id']);

    exchanges.forEach(async exchange => {
        const id = exchange.exchanges_id;

        const res = await exchangesParsers[id].parseCategories(id);

        saveCategories(res, id);
    });
};