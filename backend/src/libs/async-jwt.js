const jwt = require("jsonwebtoken");

exports.asyncJWT = {
    ...jwt,
    verify(token, secret) {
        return new Promise(function (resolve, reject) {
            jwt.verify(token, secret, function(err, data) {
                if (err) {
                    reject(err);
                }
                resolve(data)
            })
        })
    }
};