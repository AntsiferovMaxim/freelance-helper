const needle = require('needle');

module.exports = function (url) {
    return new Promise((resolve, reject) => {
        needle.get(url, (err, res) => err ? reject(err) : resolve(res));
    });
};