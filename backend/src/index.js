const express = require('express');
const path = require('path');
const morgan = require('morgan');
const bodyParser = require("body-parser");
const parseCategories = require('./libs/parse-categories');
const routes = require("./routes");
const cors = require('./middlewares/cors.middleware');

const app = express();
const port = process.env.PORT || 3000;

const pathToStatic = path.resolve(__dirname, '../public');

const server = app
    .use(express.static(pathToStatic))
    .use(bodyParser.urlencoded({extended: false}))
    .use(bodyParser.json())
    .use(cors)
    .use(morgan(':method :url :status :res[content-length] :response-time ms'))
    .use(routes)
    .listen(port, async () => {
        console.log(`Api starting in http://localhost:${port}/`);
        // parseCategories();
    });

const io = require('socket.io')(server, {origins: '*:*'});

io.on('connection', function (socket) {});
