CREATE TABLE `freelance-helper`.`users_refresh-tokens` (
  `urid` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `refresh-token` TEXT NULL,
  `agent` TEXT NULL,
  `ip` VARCHAR(45) NULL,
  PRIMARY KEY (`urid`),
  UNIQUE INDEX `urid_UNIQUE` (`urid` ASC));
