CREATE TABLE `freelance-helper`.`exchanges` (
  `exchanges_id` INT NOT NULL AUTO_INCREMENT,
  `domain` VARCHAR(255) NOT NULL,
  `name` VARCHAR(255) CHARACTER SET 'utf8' NOT NULL,
  `logo` TEXT(255) NULL,
  PRIMARY KEY (`exchanges_id`),
  UNIQUE INDEX `exchanges_id_UNIQUE` (`exchanges_id` ASC),
  UNIQUE INDEX `domain_UNIQUE` (`domain` ASC),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC));
