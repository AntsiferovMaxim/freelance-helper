-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema freelance_helper
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema freelance_helper
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `freelance_helper` DEFAULT CHARACTER SET utf8 ;
USE `freelance_helper` ;

-- -----------------------------------------------------
-- Table `freelance_helper`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `freelance_helper`.`users` (
  `user_id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(255) NOT NULL,
  `created_at` DATE NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE INDEX `user_id_UNIQUE` (`user_id` ASC),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `freelance_helper`.`refresh_tokens`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `freelance_helper`.`refresh_tokens` (
  `refresh_token_id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `user_agent` VARCHAR(45) NULL,
  PRIMARY KEY (`refresh_token_id`),
  UNIQUE INDEX `refresh_token_id_UNIQUE` (`refresh_token_id` ASC),
  INDEX `user_id_refresh_token_idx` (`user_id` ASC),
  CONSTRAINT `user_id_refresh_token`
    FOREIGN KEY (`user_id`)
    REFERENCES `freelance_helper`.`users` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
