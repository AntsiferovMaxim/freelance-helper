const instant = require('../connection');

const TABLE_NAME = 'exchanges';

class ExchangesModel {
    static getByName(name, fields = []) {
        return instant.select(...fields).from(TABLE_NAME).where('name', name).first().timeout(1000, {cancel: true})
    }
    static getAll(fields = []) {
        return instant.select(...fields).from(TABLE_NAME).timeout(1000, {cancel: true});
    }
    static updateById(id, data) {
        return instant(TABLE_NAME).where('exchanges_id', id).update(data)
    }
}

exports.ExchangesModel = ExchangesModel;
