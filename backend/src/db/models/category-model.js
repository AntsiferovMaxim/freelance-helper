const instant = require('../connection');

const CATEGORIES_TABLE = 'categories';

class CategoryModel {
    constructor(exchangeId) {
        this.exchangeId = exchangeId;
    }
    async saveOrUpdate(payload) {
        try {
            await instant.raw(instant(CATEGORIES_TABLE).insert({...payload, exchanges_id: this.exchangeId}).toQuery() + ` ON DUPLICATE KEY UPDATE link='${payload.link}', name='${payload.name}'`);
            const category = await instant.select().from(CATEGORIES_TABLE).where('link', payload.link).first();

            return category.categories_id;
        } catch (e) {
            throw new Error(e);
        }
    }
    static async getBy(where) {
        try {
            return instant.select().from(CATEGORIES_TABLE).where(where).timeout(1000, {cancel: true})
        } catch (e) {
            throw new Error(e);
        }
    }
}

exports.CategoryModel = CategoryModel;