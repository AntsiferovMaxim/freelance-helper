const instant = require("../connection");

const USERS_TABLE = 'users';
const REFRESH_TOKENS_TABLE = 'refresh_tokens';

exports.UserModel = {
    save(payload, fields = []) {
        return instant(USERS_TABLE).returning(...fields).insert(payload);
    },
    getById(id, fields = []) {
        return instant.select(...fields).from(USERS_TABLE).where('user_id', id).first().timeout(1000, {cancel: true});
    },
    getByEmail(email) {
        return instant.select().from(USERS_TABLE).where('email', email).first().timeout(1000, {cancel: true});
    },
    deleteById(id) {
        return instant(USERS_TABLE).where('user_id', id).del();
    },
    addRefreshToken(payload, fields = []) {
        return instant(REFRESH_TOKENS_TABLE).returning(...fields).insert(payload);
    },
    updateRefreshToken(client_id, token) {
        return instant(REFRESH_TOKENS_TABLE).where('client_id', client_id).update({refresh_token: token})
    },
    getRefreshTokenByClientId(client_id) {
        return instant.select('refresh_token').from(REFRESH_TOKENS_TABLE).where("client_id", client_id).first().timeout(1000, {cancel: true});
    },
    getRefreshToken(client_id, token) {
        return instant.select('refresh_token').from(REFRESH_TOKENS_TABLE).where("client_id", client_id).andWhere('refresh_token', token).first().timeout(1000, {cancel: true});
    },
    deleteRefreshToken(client_id) {
        return instant(REFRESH_TOKENS_TABLE).where('client_id', client_id).del();
    },
};